<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ore extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'dangerousness', 'description',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the ore record associated with the area.
     */
    public function area()
    {
        return $this->belongsToMany('App\Area');
    }
}
