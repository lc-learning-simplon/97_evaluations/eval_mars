<?php

namespace App\Http\Controllers;

use App\Ore;
use App\Area;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;

class OreController extends Controller
{
    public function index() {
        return view('ores', ['ores' => Ore::all()]);
    }

    public function edit($id) {
        // Liste les ore_id déjà signalé dans la zone
        $area_ore = [];
        foreach(Area::find($id)->ore()->get() as $ore) {
            $area_ore[] = $ore->id;
        }

        return view('edit', ['area' => Area::find($id), 'ores' => Ore::all(), 'area_ore' => $area_ore]);
    }

    public function report(Request $request, Redirector $redirect) {
        // Min 1 checkbox
        if ($request->input('ores')) {
            // Check for add or not
            foreach ($request->input('ores') as $ore) {
                if (Area::find($request->input('id'))->ore()->where('ore_id', $ore)->first()) {
                    // Nothing
                } else {
                    DB::table('area_ore')->insert(
                        ['area_id' => $request->input('id'), 'ore_id' => $ore]
                    );
                }
            }
            // Check for delete
            foreach (Area::find($request->input('id'))->ore()->get() as $ore) {
                if (!in_array($ore->id, $request->input('ores'))) {
                    DB::table('area_ore')->where('area_id', $request->input('id'))->where('ore_id', $ore->id)->delete();
                }
            }
        } 
        // No checkbox
        else { 
            DB::table('area_ore')->where('area_id', $request->input('id'))->delete();
        }

        return $redirect->to('/home')->with('success', 'Merci, votre signalement peut sauver des vie.');
    }

    public function addore() {

        return view('addore');
    }

    public function saveore(Request $request, Redirector $redirect) {

        // Create new ore with the ORM
        $ore = new Ore;
        $ore->name = $request->input('name');
        $ore->dangerousness = $request->input('dangerousness');
        $ore->description = $request->input('description');
        $ore->save();

        return $redirect->to('/ore')->with('success', 'Merci, votre signalement peut sauver des vie.');
    }
}
