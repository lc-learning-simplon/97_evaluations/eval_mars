<?php

namespace App\Http\Controllers;

use App\Ore;
use App\Area;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;

class AreaController extends Controller
{
    public function index()
    {
        return view('addarea', ['ores' => Ore::all()]);
    }

    public function addarea(Request $request, Redirector $redirect)
    {
        // Check if input not null
        if(!is_int($request->input('x_coord')) && !is_int($request->input('y_coord'))) {
            $exist = false;
            foreach(Area::all() as $area) {
                if($area->x_coord == $request->input('x_coord') && $area->y_coord == $request->input('y_coord')) {
                    $exist = true;
                }
            }

            if($exist == false) {
                // Create new area with the ORM
                $area = new Area;
                $area->x_coord = $request->input('x_coord');
                $area->y_coord = $request->input('y_coord');
                $area->save();
        
                // Add relationship with ores
                // Min 1 checkbox
                if ($request->input('ores')) {
                    foreach ($request->input('ores') as $ore) {
                            DB::table('area_ore')->insert(['area_id' => $area->id, 'ore_id' => $ore]);
                    }
                } 
                return $redirect->to('/home')->with('success', 'Nouvelle zone ajouté. Les Néomartiens vous remercient !');
            }
            else {
                return $redirect->back()->with('error', 'Cette zone existe déjà. Vous pouvez l\'éditer via la page d\'accueil.');
            }
        }
        else {
            return $redirect->back()->with('error', 'Vous devez indiquer des coordonnées.');
        }
    }
}
