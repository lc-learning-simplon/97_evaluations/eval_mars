<?php

namespace App\Http\Controllers;

use App\Ore;
use App\Area;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // Calcule de la dangerosité
        $danger = [];
        foreach(Area::all() as $area) {
            $danger[$area->id] = 1;
            foreach(Area::find($area->id)->ore()->get() as $ore) {
                $danger[$area->id] += $ore->dangerousness;
            }
        }
        

        return view('home', ['areas' => Area::all(), 'danger' => $danger]);
    }

}
