<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'x_coord', 'y_coord',
    ];

    /**
     * Get the area record associated with the ore.
     */
    public function ore()
    {
        return $this->belongsToMany('App\Ore');
    }
}
