<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return redirect()->to('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/addarea', 'AreaController@index');

Route::post('/addarea', 'AreaController@addarea');

Route::get('/ore', 'OreController@index');

Route::get('/ore/add', 'OreController@addore');

Route::post('/ore/add', 'OreController@saveore');

Route::get('/edit/{id}', 'OreController@edit');

Route::post('/report', 'OreController@report');

Route::get('/mentions', function() {
    return view('mentions');
});
