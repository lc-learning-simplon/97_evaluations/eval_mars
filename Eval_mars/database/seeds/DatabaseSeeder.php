<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ores')->insert([
            'name' => 'Klingon',
            'dangerousness' => 2,
            'description' => 'Le Klingon, un peu relou, altère les combinaisons, traverse les matériaux et provoque des démangeaisons cutanées,  On a observé une résistance chez les geeks.',
        ]);

        DB::table('ores')->insert([
            'name' => 'Chomdû',
            'dangerousness' => 3,
            'description' => 'Le Chomdû est un minerai qui provoque des états dépressifs. Bizzarement, les personnes âgées ne sont plus affectées.',
        ]);

        DB::table('ores')->insert([
            'name' => 'Perl',
            'dangerousness' => 4,
            'description' => 'Le Perl est le minerai à éviter à tout prix ! Hystérie, dépression, folie, voir mort subite sont les effets fréquemment observés.',
        ]);

        DB::table('areas')->insert([
            'x_coord' => 0,
            'y_coord' => 0,
        ]);
    }
}
