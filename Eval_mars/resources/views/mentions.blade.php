@extends('layouts.app')

@section('content')

<style>
    .eye {
        position: relative;
        width: 100px;
        height: 50px;
        border-radius: 50%;
        border-top: 1px solid rgba(0, 0, 0, 0.5);
        border-bottom: 1px solid rgba(0, 0, 0, 0.5);
        overflow: hidden;
        animation: 3s ease-in close infinite;
    }

    .eye::after {
        content: '';
        position: absolute;
        left: 20px;
        top: calc(50% - 27px);
        width: 10px;
        height: 10px;
        border: 25px solid rgba(238, 162, 20, 0.9);
        border-radius: 50%;
        background: white;
        animation: 3s ease-in eye infinite;
    }

    @keyframes close {

        0%,
        35%,
        55%,
        100% {
            height: 50px;
        }

        45% {
            height: 0;
        }
    }

    @keyframes eye {

        0%,
        100% {
            transform: translateX(0);
        }

        35% {
            transform: translateX(20px);
        }

        65% {
            transform: translateX(-20px);
        }
    }
</style>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-transparent">
        <li class="breadcrumb-item"><a href="/home">Accueil</a></li>
        <li class="breadcrumb-item active" aria-current="page">Mentions</li>
    </ol>
</nav>

<div class="container">
    <div class="row">
        <div class="col">
            <h5>Votre gouvernement voit tout.</h5>
            <h5>Votre gouvernement sait tout.</h5>
            <h5>Votre gouvernement ne veut que votre bien.</h5>
        </div>
        <div class="col d-flex justify-content-center align-items-center">
            <div class="eye"></div>
        </div>
    </div>
    <hr>
</div>
@endsection