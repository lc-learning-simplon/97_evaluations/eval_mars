@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent">
            <li class="breadcrumb-item"><a href="/home">Accueil</a></li>
            <li class="breadcrumb-item active" aria-current="page">Les minerais</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col">
            <h1>Les minerais</h1>
        </div>
        <div class="col">
            <div class="d-flex justify-content-end">
                <a href="/ore/add" class="btn btn-danger btn-lg">Ajouter un minerais</a>
            </div>
        </div>
    </div>
    <hr>
    @foreach($ores as $ore)
    @if($ore->dangerousness < 4) <h3><svg class="mb-2 mr-1 text-warning" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M9.022 3.566a1.13 1.13 0 011.96 0l6.857 11.667c.457.778-.092 1.767-.98 1.767H3.144c-.889 0-1.437-.99-.98-1.767L9.022 3.566zM9.002 14a1 1 0 112 0 1 1 0 01-2 0zM10 7a.905.905 0 00-.9.995l.35 3.507a.553.553 0 001.1 0l.35-3.507A.905.905 0 0010 7z" clip-rule="evenodd" />
        </svg>{{$ore->name}}</h3>
        @else
        <h3><svg class="mb-2 mr-1 text-danger" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M9.022 3.566a1.13 1.13 0 011.96 0l6.857 11.667c.457.778-.092 1.767-.98 1.767H3.144c-.889 0-1.437-.99-.98-1.767L9.022 3.566zM9.002 14a1 1 0 112 0 1 1 0 01-2 0zM10 7a.905.905 0 00-.9.995l.35 3.507a.553.553 0 001.1 0l.35-3.507A.905.905 0 0010 7z" clip-rule="evenodd" />
            </svg>{{$ore->name}}</h3>
        @endif
        <p><strong>Niveau de danger : {{$ore->dangerousness}}</strong></p>
        <p>{{$ore->description}}</p>
        <br>
        @endforeach
        <hr>
</div>
@endsection