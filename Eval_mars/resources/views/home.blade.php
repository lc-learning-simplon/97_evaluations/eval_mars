@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent">
            <li class="breadcrumb-item active" aria-current="page">Accueil</li>
        </ol>
    </nav>
    @foreach($areas as $area)
        <h3>Zone {{$area->id}}</h3>
        <p>X : {{$area->x_coord}} Y : {{$area->y_coord}}</p>
        @if($danger[$area->id] < 2) 
            <p class="text-success"><strong>Niveau de danger : {{$danger[$area->id]}}</strong></p>
        @elseif($danger[$area->id] < 8) 
            <p class="text-warning"><strong>Niveau de danger : {{$danger[$area->id]}}</strong></p>
        @else     
            <p class="text-danger"><strong>Niveau de danger : {{$danger[$area->id]}}</strong></p>
        @endif
        <form action="/edit/{{$area->id}}" method="GET">
            <button type="submit" class="btn btn-warning">Signaler un minerais</button>
        </form>
        <hr>
    @endforeach
</div>
@endsection