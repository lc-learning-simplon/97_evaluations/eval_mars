@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent">
            <li class="breadcrumb-item"><a href="/home">Accueil</a></li>
            <li class="breadcrumb-item active" aria-current="page">Zone {{$area->id}}</li>
        </ol>
    </nav>
    <h2>Zone {{$area->id}}</h2>
    <hr>
    <p><strong>Signaler la présence d'un minerais :</strong></p>
    <form action="/report" method="POST">
        @csrf
        <input type="hidden" name="id" value="{{$area->id}}">
        @foreach($ores as $ore)
        <div class="input-group">
            <label for="{{$ore->name}}">{{$ore->name}}</label>
            @if(in_array($ore->id, $area_ore))
            <input type="checkbox" class="form-check-input" id="{{$ore->name}}" name="ores[]" value="{{$ore->id}}" checked>
            @else
            <input type="checkbox" class="form-check-input" id="{{$ore->name}}" name="ores[]" value="{{$ore->id}}">
            @endif
        </div>
        @endforeach
        <button type="submit" class="btn btn-warning">Signaler</button>
    </form>
</div>
@endsection