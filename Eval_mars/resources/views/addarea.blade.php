@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent">
            <li class="breadcrumb-item"><a href="/home">Accueil</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ajouter une zone</li>
        </ol>
    </nav>
    <h1>Déclarer une nouvelle zone</h1>
    <hr>
    <div class="form-group col-2">
        <form action="/addarea" method="POST">
            @csrf

            <p><strong>Localisation :</strong></p>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">X</div>
                </div>
                <input type="number" class="form-control" id="x_coord" name="x_coord" value="0" require>
            </div>

            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Y</div>
                </div>
                <input type="number" class="form-control" id="y_coord" name="y_coord" value="0" require>
            </div>

            <p><strong>Signaler minerais :</strong></p>
            @foreach($ores as $ore)
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="{{$ore->name}}" name="ores[]" value="{{$ore->id}}">
                <label for="{{$ore->name}}" class="form-check-label">{{$ore->name}}</label>
            </div>
            @endforeach

            <button type="submit" class="btn btn-info mt-2 text-white">Valider</button>
        </form>
    </div>
    <hr>
</div>
@endsection