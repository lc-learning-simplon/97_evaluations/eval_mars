@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent">
            <li class="breadcrumb-item"><a href="/home">Accueil</a></li>
            <li class="breadcrumb-item"><a href="/ore">Les minerais</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ajouter un minerais</li>
        </ol>
    </nav>

    <div class="form-group col-4">
        <form action="/ore/add" method="POST">
            @csrf

            <div class="form-group">
                <label for="name"><strong>Nom du minerais :</strong></label>
                <input type="text" class="form-control" name="name" id="name">
            </div>
            <div class="form-group">
                <label for="dangerousness"><strong>Niveau de danger :</strong></label>
                <select class="form-control" name="dangerousness" id="dangerousness">
                    <option value="2">2 (Danger faible)</option>
                    <option value="3">3 (Danger moyen)</option>
                    <option value="4">4 (Danger élevé)</option>
                </select>
            </div>
            <div class="form-group">
                <label for="description"><strong>Description :</strong></label>
                <textarea class="form-control" name="description" id="description"></textarea>
            </div>

            <button type="submit" class="btn btn-info mt-2 text-white">Valider</button>
        </form>
    </div>

    <hr>
</div>
@endsection