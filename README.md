![mars](https://s2.qwant.com/thumbr/0x0/d/e/e3f621236c40daba99faecd3217e742265d47fd9b763f59bc5c8f41f9425a2/giphy.gif?u=https%3A%2F%2Fmedia.giphy.com%2Fmedia%2FzlHp8vbztNTFe%2Fgiphy.gif&q=0&b=1&p=0&a=1)

# Let's colonize Mars !
Lucas CHEVALLIER

Créez une application collaborative qui permette à nos congénères d'éviter les zones radioactives sur Mars ! Ce projet met en oeuvre l'ensemble de vos compétences : front, back, DB, gestion de projet...

## QUICK START
1. `git clone`.
2. `composer install`.
3. `npm install && npm run dev`.
4. Configurez votre `.env` selon le modèle.
5. `php artisan migrate`
6. `php artisan db:seed`
7. `php artisan serve`

Enjoy !


## USERS STORIES
En tant qu'utilisateur je veux :
- me créer un profil.
- visualiser les zones.
- consulté les informations de chaque zone.
- éditer les informations de chaque zone.
- déclarer la présence d'un nouveau minerai.
- ajouter une zone.


## LIEN PRODUCTION
[Let's conlonize mars](http://let-s-colonize-mars.herokuapp.com)

